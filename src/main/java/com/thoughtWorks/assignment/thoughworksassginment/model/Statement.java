package com.thoughtWorks.assignment.thoughworksassginment.model;

public class Statement {

    String romanNumber;
    String creditName;
    String creditValue;

    public Statement(String romanNumber, String creditName, String creditValue){
        this.creditName = creditName;
        this.creditValue = creditValue;
        this.romanNumber = romanNumber;
    }
    public String getRomanNumber() {
        return romanNumber;
    }

    public String getCreditName() {
        return creditName;
    }

    public String getCreditValue() {
        return creditValue;
    }
}

package com.thoughtWorks.assignment.thoughworksassginment.constants;

public final class Constants {

    public static final String credits = "Credits";
    public static final String nonCredits = "NonCredits";
    public static final String space = " ";
    public static final String spaceRegEx = "\\s";
    public static final String questionMark = " ?";
    public static final String is = "is";
    public static final String many_credits = "many Credits";
    public static final String much = "much";
    public static final String errorMsg = "I have no idea what you are talking about";
    public static final String how = "how ";
    public static final String validRomanNumber ="^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";
}

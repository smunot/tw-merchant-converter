package com.thoughtWorks.assignment.thoughworksassginment.constants;

public enum RomanValue {
    I(1),
    V(5),
    X(10),
    L(50),
    C(100),
    D(500),
    M(1000);

    public final Integer label;

    private RomanValue(Integer label) {
        this.label = label;
    }
}

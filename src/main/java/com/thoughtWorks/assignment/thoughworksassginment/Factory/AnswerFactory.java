package com.thoughtWorks.assignment.thoughworksassginment.Factory;

import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;
import com.thoughtWorks.assignment.thoughworksassginment.implementation.CreditAnswer;
import com.thoughtWorks.assignment.thoughworksassginment.implementation.NonCreditAnswer;
import com.thoughtWorks.assignment.thoughworksassginment.services.AnswerTheQuestion;

public class AnswerFactory {

    private static CreditAnswer creditAnswer = null;
    private static NonCreditAnswer nonCreditAnswer = null;

    static {
        creditAnswer = new CreditAnswer();
        nonCreditAnswer = new NonCreditAnswer();
    }

    public static AnswerTheQuestion getAnswerObj(String className) {
        if (Constants.credits.equals(className)) {
            return creditAnswer;
        } else if (Constants.nonCredits.equals(className)) {
            return nonCreditAnswer;
        }
        return null;
    }

}

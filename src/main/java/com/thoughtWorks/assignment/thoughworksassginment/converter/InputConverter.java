package com.thoughtWorks.assignment.thoughworksassginment.converter;

import com.thoughtWorks.assignment.thoughworksassginment.Factory.AnswerFactory;
import com.thoughtWorks.assignment.thoughworksassginment.Factory.InputFactory;
import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;
import com.thoughtWorks.assignment.thoughworksassginment.constants.RomanValue;
import com.thoughtWorks.assignment.thoughworksassginment.implementation.Credit;
import com.thoughtWorks.assignment.thoughworksassginment.implementation.CreditAnswer;
import com.thoughtWorks.assignment.thoughworksassginment.implementation.NonCredit;
import com.thoughtWorks.assignment.thoughworksassginment.implementation.NonCreditAnswer;
import com.thoughtWorks.assignment.thoughworksassginment.validation.Question;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.function.Predicate;

public class InputConverter {


    Question question = new Question();

    public StringBuffer processFile(List<String> lines) {

        StringBuffer output = new StringBuffer();
        lines.forEach(line -> {
            if (line.startsWith(Constants.how)) {
                output.append(AnswerTheQuestions(line)).append('\n');
            } else if (line.endsWith(Constants.credits)) {
                mapCreditsValue(line);
            } else {
                Boolean isValid = false;
                for (RomanValue c : RomanValue.values()) {
                    if (line.contains("is " + c.name())) {
                        isValid = true;
                        break;
                    }
                }
                if (isValid) {
                    mapNonCreditValue(line);
                } else {
                    output.append(Constants.errorMsg);
                }
            }
        });
        return output;
    }

    private void mapNonCreditValue(String line) {
        if (!StringUtils.isEmpty(line)) {
            InputFactory.getInputObj(Constants.nonCredits).mapValues(line);
//            ((NonCredit)merchantFactory.getFactory("Input",Constants.nonCredits)).mapValues(line);
        }
    }

    private void mapCreditsValue(String line) {
        if (!StringUtils.isEmpty(line)) {
//            ((Credit)merchantFactory.getFactory("Input",Constants.credits)).mapValues(line,
//                    ((NonCredit)merchantFactory.getFactory("Input",Constants.nonCredits)).getNonCreditValues());

            InputFactory.getInputObj(Constants.credits).mapValues(line,
                    ((NonCredit) InputFactory.getInputObj(Constants.nonCredits)).getNonCreditValues());
        }
    }

    private String AnswerTheQuestions(String line) {
//        NonCredit nonCredit = (NonCredit) merchantFactory.getFactory("Input",Constants.nonCredits);
//        Credit credit = (Credit) merchantFactory.getFactory("Input",Constants.credits);
        NonCredit nonCredit = (NonCredit) InputFactory.getInputObj(Constants.nonCredits);
        Credit credit = (Credit) InputFactory.getInputObj(Constants.credits);
        String questionType = question.validateQuestions(line, credit.getRomanCreditValue(), nonCredit.getNonCreditValues());
        if (null != questionType) {
            if (questionType.equals(Constants.credits)) {
//                return ((CreditAnswer)merchantFactory.getFactory("Answer",Constants.credits)).printAnswer(line, nonCredit.getNonCreditValues(), credit.getRomanCreditValue());
                return AnswerFactory.getAnswerObj(Constants.credits).printAnswer(line, nonCredit.getNonCreditValues(), credit.getRomanCreditValue());
            } else if (questionType.equals(Constants.nonCredits)) {
                return AnswerFactory.getAnswerObj(Constants.nonCredits).printAnswer(line, nonCredit.getNonCreditValues());
//                return ((NonCreditAnswer)merchantFactory.getFactory("Answer",Constants.nonCredits)).printAnswer(line, nonCredit.getNonCreditValues());
            }
            return Constants.errorMsg;
        }
        return Constants.errorMsg;
    }
}

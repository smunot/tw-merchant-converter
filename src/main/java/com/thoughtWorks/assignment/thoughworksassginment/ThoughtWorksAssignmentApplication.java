package com.thoughtWorks.assignment.thoughworksassginment;

import com.thoughtWorks.assignment.thoughworksassginment.converter.InputConverter;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
@SpringBootApplication
@SpringBootConfiguration
class ThoughtWorksAssignmentApplication {
    private static final Logger LOGGER = Logger.getLogger(ThoughtWorksAssignmentApplication.class.getName());
    public static void main(String[] args) {

        System.out.println("Enter Full path of File");
        String filename = null;
        try {
            Scanner keyboard = new Scanner(System.in);
            filename = keyboard.nextLine();
            filename = filename.replace("\\","\\\\");

            List<String> lines = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
            InputConverter inputConverter = new InputConverter();
            StringBuffer output = inputConverter.processFile(lines);
            System.out.println(output);
        }catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING,filename);
        }
        catch (IOException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING,"Some other Exception");
        }
    }
}

package com.thoughtWorks.assignment.thoughworksassginment.validation;

import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;

import java.math.BigDecimal;
import java.util.Map;

public class Question {



    public String validateQuestions(String line, Map<String, BigDecimal> romanCreditValue, Map<String, String> romanValues) {
        Boolean isCreaditKeyContains = false;
        Boolean isValuesKeyContains = false;
        for (Map.Entry<String, BigDecimal> e : romanCreditValue.entrySet()) {
            if (line.contains(e.getKey())) {
                isCreaditKeyContains = true;
            }
        }
        for (Map.Entry<String, String> e : romanValues.entrySet()) {
            if (line.contains(e.getKey())) {
                isValuesKeyContains = true;
            }
        }
        return (line.contains(Constants.many_credits) && isCreaditKeyContains) ? Constants.credits :
                (line.contains(Constants.much) && isValuesKeyContains) ? Constants.nonCredits :null;
    }
}

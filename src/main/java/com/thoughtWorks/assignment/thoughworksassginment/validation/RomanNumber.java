package com.thoughtWorks.assignment.thoughworksassginment.validation;

import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;
import com.thoughtWorks.assignment.thoughworksassginment.constants.RomanValue;

import java.math.BigDecimal;

public class RomanNumber {

    String romanNumber;
    public RomanNumber(String romanNumber){
        this.romanNumber = romanNumber;
    }
    public String getRomanNumber() {
        return romanNumber;
    }
    public boolean validateRomanNumber() {
        if (!this.romanNumber.matches(Constants.validRomanNumber)) {
            System.out.println(romanNumber + " is not a valid roman value");
            return false;
        }
        return true;
    }
    private Boolean validateValues(String s, String s1) {
        if ("I".equals(s) && "VX".contains(s1)) {
            return true;
        }
        if ("X".equals(s) && "LC".contains(s1)) {
            return true;
        }

        if ("C".equals(s) && "DM".contains(s1)) {
            return true;
        }

        if ("VLD".contains(s)) {
            return false;
        }
        return false;
    }


    public  BigDecimal calculateDecimalValue() {
        int[] values = new int[getRomanNumber().length()];
        if (!validateRomanNumber()) {
            return BigDecimal.ZERO;
        }
        for (int i = 0; i < getRomanNumber().length(); i++) {
            values[i] = RomanValue.valueOf(String.valueOf(getRomanNumber().charAt(i))).label;
        }
        BigDecimal sum = BigDecimal.ZERO;
        for (int i = 0; i < values.length; i++) {
            if ((i + 1) == values.length || values[i] >= values[i + 1]) {
                sum = sum.add(new BigDecimal(values[i]));
            } else {

                if (validateValues(String.valueOf(getRomanNumber().charAt(i)), String.valueOf(getRomanNumber().charAt(i + 1)))) {
                    sum = sum.add(new BigDecimal(values[i + 1] - values[i]));
                    i++;
                }
            }
        }
        return sum;
    }
}

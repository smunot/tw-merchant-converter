package com.thoughtWorks.assignment.thoughworksassginment.implementation;

import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;
import com.thoughtWorks.assignment.thoughworksassginment.services.AnswerTheQuestion;
import com.thoughtWorks.assignment.thoughworksassginment.validation.RomanNumber;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

public class NonCreditAnswer implements AnswerTheQuestion {

    @Override
    public String printAnswer(String line, Map<String, String> nonCredit, Map<String, BigDecimal> Credit) {
        return null;
    }

    @Override
    public String printAnswer(String line, Map<String, String> nonCreditMap) {
        String statement = createStatement(line);
        NonCredit nonCredit = new NonCredit();
        RomanNumber romanNumber = new RomanNumber(nonCredit.createRomanNumberFromNonCredit(statement, nonCreditMap));
        BigDecimal result = romanNumber.calculateDecimalValue();
        if(BigDecimal.ZERO.compareTo(result) != 0) {
            return (statement + " is " + result);
        }else{
            return Constants.errorMsg;
        }
    }


    private String createStatement(String line) {
        line = line.replace(Constants.questionMark, "");
        String[] words = line.split(Constants.spaceRegEx);
        int isIndex = Arrays.asList(words).indexOf(Constants.is);
        StringBuffer romanNumber = new StringBuffer();
        romanNumber.append(line.substring(line.indexOf(words[isIndex + 1]), line.length()));
        return romanNumber.toString();
    }
}

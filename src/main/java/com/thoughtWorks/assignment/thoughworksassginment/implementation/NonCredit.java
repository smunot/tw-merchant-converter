package com.thoughtWorks.assignment.thoughworksassginment.implementation;

import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;
import com.thoughtWorks.assignment.thoughworksassginment.services.InputService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class NonCredit implements InputService {

    public Map<String, String> getNonCreditValues() {
        return nonCreditValues;
    }

    public Map<String, String> nonCreditValues = new HashMap<>();

    @Override
    public void mapValues(String line) {
        String[] words = line.split(Constants.spaceRegEx);
        nonCreditValues.putIfAbsent(words[0], words[2]);
    }

    @Override
    public void mapValues(String line, Map<String, String> nonCredit) {

    }

    public String createRomanNumberFromNonCredit(String line, Map<String, String> nonCredit) {
        String[] words = line.split(Constants.spaceRegEx);
        StringBuffer romanNumber = new StringBuffer();
        Arrays.stream(words).forEach(x -> romanNumber.append(nonCredit.get(x)));
        return romanNumber.toString();
    }
}

package com.thoughtWorks.assignment.thoughworksassginment.implementation;

import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;
import com.thoughtWorks.assignment.thoughworksassginment.model.Statement;
import com.thoughtWorks.assignment.thoughworksassginment.services.InputService;
import com.thoughtWorks.assignment.thoughworksassginment.validation.RomanNumber;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Credit implements InputService {

    public Map<String, BigDecimal> romanCreditValue = new HashMap<>();

    public Map<String, BigDecimal> getRomanCreditValue() {
        return romanCreditValue;
    }

    @Override
    public void mapValues(String line) {

    }

    @Override
    public void mapValues(String line, Map<String, String> nonCredit) {
        Statement statement = createStatement(line);
        RomanNumber romanNumber = new RomanNumber(createRomanNumberFromCredit(statement.getRomanNumber(), nonCredit));

        if (romanNumber.validateRomanNumber()) {
            BigDecimal intValue = romanNumber.calculateDecimalValue();
            romanCreditValue.putIfAbsent(statement.getCreditName(), BigDecimal.ZERO.add(new BigDecimal(statement.getCreditValue())).divide(intValue));
        }
    }

    private Statement createStatement(String line) {
        String[] words = line.split(Constants.spaceRegEx);
        int isIndex = Arrays.asList(words).indexOf(Constants.is);
        String creditName = words[isIndex - 1];
        String creditValue = words[isIndex + 1];
        String romanNumber = line.substring(0, line.indexOf(words[isIndex - 1]));
        return new Statement(romanNumber, creditName, creditValue);
    }


    private String createRomanNumberFromCredit(String line, Map<String, String> nonCredit) {
        String[] words = line.split(Constants.spaceRegEx);
        StringBuffer romanNumber = new StringBuffer();
        Arrays.stream(words).forEach(word -> romanNumber.append(nonCredit.get(word)));
        return romanNumber.toString();
    }
}

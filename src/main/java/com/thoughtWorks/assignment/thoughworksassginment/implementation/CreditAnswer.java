package com.thoughtWorks.assignment.thoughworksassginment.implementation;

import com.thoughtWorks.assignment.thoughworksassginment.constants.Constants;
import com.thoughtWorks.assignment.thoughworksassginment.model.Statement;
import com.thoughtWorks.assignment.thoughworksassginment.services.AnswerTheQuestion;
import com.thoughtWorks.assignment.thoughworksassginment.validation.RomanNumber;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

public class CreditAnswer implements AnswerTheQuestion {

    @Override
    public String printAnswer(String line, Map<String, String> nonCreditMap, Map<String, BigDecimal> credit) {
        Statement statement = createStatement(line);
        NonCredit nonCredit = new NonCredit();
        RomanNumber romanNumber = new RomanNumber(nonCredit.createRomanNumberFromNonCredit(statement.getRomanNumber(), nonCreditMap));
        BigDecimal multiplier = romanNumber.calculateDecimalValue();
        BigDecimal result = multiplier.multiply(credit.get(statement.getCreditName()));
        if(BigDecimal.ZERO.compareTo(multiplier) != 0) {
            return (statement.getRomanNumber() + " " + statement.getCreditName() + " is " + result.intValue() + " Credits");
        }else{
            return Constants.errorMsg;
        }
    }

    @Override
    public String printAnswer(String line, Map<String, String> nonCredit) {
        return null;
    }

    private Statement createStatement(String line) {
        line = line.replace(Constants.questionMark, "");
        String[] words = line.split(Constants.spaceRegEx);
        int isIndex = Arrays.asList(words).indexOf(Constants.is);
        String creditName = null;
        StringBuffer romanNumber = new StringBuffer();
        String s = line.substring(line.indexOf(words[isIndex + 1]));
        romanNumber.append(s.substring(0, s.lastIndexOf(Constants.space)));
        creditName = s.substring(s.lastIndexOf(Constants.space) + 1, s.length());
        return new Statement(romanNumber.toString(), creditName, null);
    }
}

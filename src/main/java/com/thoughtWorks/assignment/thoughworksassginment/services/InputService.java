package com.thoughtWorks.assignment.thoughworksassginment.services;


import java.util.Map;

public interface InputService {

    public void mapValues(String line);
    public void mapValues(String line, Map<String,String> nonCredit);
}

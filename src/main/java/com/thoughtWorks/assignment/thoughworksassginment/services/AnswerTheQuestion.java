package com.thoughtWorks.assignment.thoughworksassginment.services;

import java.math.BigDecimal;
import java.util.Map;

public interface AnswerTheQuestion {
    public String printAnswer(String line, Map<String, String> nonCredit,Map<String, BigDecimal> Credit);
    public String printAnswer(String line, Map<String, String> nonCredit);
}

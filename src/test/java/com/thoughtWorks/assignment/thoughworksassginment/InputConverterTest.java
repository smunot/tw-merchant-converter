package com.thoughtWorks.assignment.thoughworksassginment;

import com.thoughtWorks.assignment.thoughworksassginment.converter.InputConverter;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

public class InputConverterTest {

InputConverter inputConverter = new InputConverter();

    @Test
    public void testPrintMessageForGivenScenarioInDocument() {
    List<String> input = prepareInputCollection();
        String expectedoutput = "pish tegj glob glob is 42" + '\n' +
                "glob prok Silver is 68 Credits" + '\n' +
                "glob prok Gold is 57800 Credits" + '\n' +
                "glob prok Iron is 782 Credits" + '\n' +
                "I have no idea what you are talking about" + '\n';
        Assert.isTrue(expectedoutput.equals(inputConverter.processFile(input).toString()));
    }

    @Test
    public void testPrintMessageForXXXX() {
        List<String> input = prepareInputCollectionForXXXX();
        Assert.isTrue(("I have no idea what you are talking about" + '\n').equals(inputConverter.processFile(input).toString()));
    }

    @Test
    public void testPrintMessageForRandomInput() {
        List<String> input = new ArrayList<>();
        input.add("ghjgjh is jhkjh");
        Assert.isTrue("I have no idea what you are talking about".equals(inputConverter.processFile(input).toString()));
    }



    private List<String> prepareInputCollectionForXXXX() {
        List<String> input = new ArrayList<>();
        input.add("pish is X");
        input.add("how much is pish pish pish pish ?");
        return input;
    }


    private List<String> prepareInputCollection() {
        List<String> input = new ArrayList<>();
        input.add("glob is I");
        input.add("prok is V");
        input.add("pish is X");
        input.add("tegj is L");
        input.add("glob glob Silver is 34 Credits");
        input.add("glob prok Gold is 57800 Credits");
        input.add("pish pish Iron is 3910 Credits");
        input.add("how much is pish tegj glob glob ?");
        input.add("how many Credits is glob prok Silver ?");
        input.add("how many Credits is glob prok Gold ?");
        input.add("how many Credits is glob prok Iron ?");
        input.add("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
        return input;
    }
}
